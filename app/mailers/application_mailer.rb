class ApplicationMailer < ActionMailer::Base
  default from: 'ashutosh.iiitm.19@gmail.com'
  layout 'mailer'
end
